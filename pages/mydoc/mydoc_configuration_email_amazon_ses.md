---
title: Email configuration with Amazon SES
summary: "Configuration of email delivery with Amazon SES"
tags: [configuration, email]
sidebar: mydoc_sidebar
permalink: mydoc_configuration_email_amazon_ses.html
folder: mydoc
---

## Preamble

The server supports multiple email providers. This guide will explain how to configure the Psono server to use Amazon SES for
email delivery.

## Configuration

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Configure email address

	```
	EMAIL_FROM: 'something@example.com'
	```

	All emails that are sent by the server will come from this email address.

    {% include note.html content="Restart the server afterwards" %}

2.  Add Amazon SES API credentials to setting.yml

	```
	EMAIL_BACKEND: 'anymail.backends.amazon_ses.EmailBackend'
	AMAZON_SES_CLIENT_PARAMS_ACCESS_KEY_ID: 'YOUR_AMAZON_SES_CLIENT_PARAMS_ACCESS_KEY_ID'
	AMAZON_SES_CLIENT_PARAMS_SECRET_ACCESS_KEY: 'YOUR_AMAZON_SES_CLIENT_PARAMS_SECRET_ACCESS_KEY'
	AMAZON_SES_CLIENT_PARAMS_REGION_NAME: 'YOUR_AMAZON_SES_CLIENT_PARAMS_REGION_NAME'
	```

	- Replace YOUR_AMAZON_SES_CLIENT_PARAMS_ACCESS_KEY_ID with the access key id that was provided to you by Amazon.
	- Replace YOUR_AMAZON_SES_CLIENT_PARAMS_SECRET_ACCESS_KEY with the secret access key that was provided to you by Amazon.
	- Replace YOUR_AMAZON_SES_CLIENT_PARAMS_REGION_NAME with the region name that was provided to you by Amazon, e.g. us-west-2

    {% include note.html content="Restart the server afterwards" %}


## Testing

To send a test email to `something@something.com` execute:

    python3 ./psono/manage.py sendtestmail something@something.com

or with docker:

    docker run --rm \
      -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-server:latest python3 ./psono/manage.py sendtestmail something@something.com

If you receive this test email, then email should be configured proper.


## More Information

Psono is using Anymail under the hood. You can check out the official documentation here:

[anymail.readthedocs.io/en/stable/esps/amazon_ses/](https://anymail.readthedocs.io/en/stable/esps/amazon_ses/)


{% include links.html %}
