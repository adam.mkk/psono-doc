---
title: Email configuration with Mailgun
summary: "Configuration of email delivery with Mailgun"
tags: [configuration, email]
sidebar: mydoc_sidebar
permalink: mydoc_configuration_email_mailgun.html
folder: mydoc
---

## Preamble

The server supports multiple email providers. This guide will explain how to configure the Psono server to use Mailgun for
email delivery.

## Configuration

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Configure email address

	```
	EMAIL_FROM: 'something@example.com'
	```

	All emails that are sent by the server will come from this email address.

    {% include note.html content="Restart the server afterwards" %}

2.  Add Mailgun API credentials to setting.yml

	```
	EMAIL_BACKEND: 'anymail.backends.mailgun.EmailBackend'
    MAILGUN_ACCESS_KEY: 'MAILGUN_ACCESS_KEY'
	```

	Replace MAILGUN_ACCESS_KEY with the API key that was provided to you by Mailgun.

    {% include note.html content="Restart the server afterwards" %}


3.  EMAIL_FROM's domain and Mailgun's domain differ

    If you are want to use an email address with a domain that does not match the one that you configured with Mailgun, you can configure here a different one.

	```
	MAILGUN_SERVER_NAME: 'MAILGUN_SERVER_NAME'
	```

	Replace MAILGUN_SERVER_NAME with the domain that you registered with Mailgun. This is only required if your Maingun sender domain is different from your EMAIL_FROM's domain. This setting is not needed if the domain of EMAIL_FROM matches the one from Mailgun.


    {% include note.html content="Restart the server afterwards" %}


4.  (optional) EU Server

    If you are using the EU server, you have to specify in addition

	```
	MAILGUN_API_URL: 'https://api.eu.mailgun.net/v3'
	```

    {% include note.html content="Restart the server afterwards" %}


## Testing

To send a test email to `something@something.com` execute:

    python3 ./psono/manage.py sendtestmail something@something.com

or with docker:

    docker run --rm \
      -v /path/to/modified/settings.yaml:/root/.psono_server/settings.yaml \
      -ti psono/psono-server:latest python3 ./psono/manage.py sendtestmail something@something.com

If you receive this test email, then email should be configured proper.


## More Information

Psono is using Anymail under the hood. You can check out the official documentation here:

[anymail.readthedocs.io/en/stable/esps/mailgun/](https://anymail.readthedocs.io/en/stable/esps/mailgun/)


{% include links.html %}
