---
title: FreeIPA for LDAP Authentication
summary: "Configuration of LDAP with FreeIPA"
tags: [configuration, ldap]
sidebar: mydoc_sidebar
permalink: mydoc_configuration_ldap_free_ipa.html
folder: mydoc
---

## Preamble

The EE server and client support the LDAP protocol that allows you to configure an external LDAP service for authentication.
This guide here will explain how to configure Psono Server to use a FreeIPA LDAP. We assume that psono server can firewall / network wise access the LDAP Server / port.
In addition we assume that your webclient is running on https://example.com, the server is reachable with
https://example.com/server (e.g. https://example.com/server/info/ shows you some nice json output).

## Server (settings.yaml)

During the installation of the server you have created a settings.yaml that needs to be adjusted now.

1.  Standard configuration

First lets configure the standard configuration part that is the same for all LDAP connections

	```
	LDAP : [
      {
	    'LDAP_URL': 'YOUR_LDAP_URL',
	    'LDAP_DOMAIN': 'LDAP_DOMAIN',
	    'LDAP_BIND_DN': 'YOUR_LDAP_BIND_DN',
	    'LDAP_BIND_PASS': 'YOUR_LDAP_BIND_PASS',
	    'LDAP_SEARCH_USER_DN': 'YOUR_LDAP_SEARCH_USER_DN',
	    'LDAP_SEARCH_GROUP_DN': 'YOUR_LDAP_SEARCH_GROUP_DN',
        ...
      },
    ]
	```

	- Replace YOUR_LDAP_URL with the URL to your LDAP. e.g. ldaps://192.168.0.1:636
	- Replace YOUR_LDAP_DOMAIN with your LDAP's domain. e.g. example.com
	- Replace YOUR_LDAP_BIND_DN with the DN of a user in your LDAP that can browse the directory. e.g. CN=LDAPPsono,OU=UsersTech,OU=example.com,DC=example,DC=com
	- Replace YOUR_LDAP_BIND_PASS with the password fo the user specified with the LDAP_BIND_DN
	- Replace YOUR_LDAP_SEARCH_USER_DN with the root DN underneath legit user objects would reside.
	- Replace YOUR_LDAP_SEARCH_GROUP_DN with the root DN underneath legit group objects would reside.

    {% include note.html content="Restart the server afterwards" %}

2.  Adjust the setting.yml as shown below

The specific part for FreeIPA looks like this.

	```
	LDAP : [
      {
        ...
        'LDAP_ATTR_GUID': 'ipaUniqueID',
        'LDAP_OBJECT_CLASS_USER': 'posixaccount',
        'LDAP_OBJECT_CLASS_GROUP': 'posixgroup',
        'LDAP_ATTR_USERNAME': 'uid',
        'LDAP_ATTR_EMAIL': 'mail',
        'LDAP_ATTR_GROUPS': 'memberOf',
        'LDAP_MEMBER_OF_OVERLAY': True,
        'LDAP_ATTR_GROUP_MEMBER_ATTRIBUTE': 'uid',
        'LDAP_ATTR_MEMBERS': 'cn',
        ...
      },
    ]
	```
    {% include note.html content="Restart the server afterwards" %}

## Client (config.json)

Now you have to configure your client, so your users can use this configured IDP.

1.  Edit config.json

    Update your config.json similar to the one shown below.

	```json
	{
	  ...
      "authentication_methods": ["LDAP"],
	  ...
	}
	```

	The variable authentication_methods restricts the allowed login methods. In the example above only LDAP will be allowed.

{% include links.html %}
